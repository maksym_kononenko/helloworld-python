#!/bin/bash

set -e

mkdir /opt/repo-dev/
mkdir /opt/SysOps.git
cd /opt/repo-dev/
git clone https://maksym_kononenko@bitbucket.org/maksym_kononenko/helloworld-python.git
cd ./helloworld-python
git remote add prod /opt/SysOps.git
cd /opt/SysOps.git/
git init
git config receive.denyCurrentBranch ignore
chmod -R +x /opt/repo-dev/helloworld-python/ci-cd/
cp /opt/repo-dev/helloworld-python/ci-cd/post-update /opt/SysOps.git/.git/hooks/
echo "*/1 * * * * root /opt/repo-dev/helloworld-python/ci-cd/cron.sh" >> /etc/crontab
service cron reload