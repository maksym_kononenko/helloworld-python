#!/usr/bin/env bash

set -e

export DEBIAN_FRONTEND=noninteractive

fatal()
{
    echo "fatal: $@" >&2
}

check_for_root()
{
    if [[ $EUID != 0 ]]; then
        fatal "need to be root"
        exit 1
    fi
}

check_for_gitlab_rb()
{
    if [[ ! -e /vagrant/ci-cd/hosts ]]; then
        fatal "hosts not found at /vagrant"
        exit 1
    fi
}

# All commands expect root access.
check_for_root

# Update the hosts
mkdir -p /root/.ssh
yes | cp -f /vagrant/ci-cd/hosts /etc/hosts
yes | cp -f /vagrant/ci-cd/ssh_keys/id_rsa* /root/.ssh/
cat /root/.ssh/id_rsa.pub > /root/.ssh/authorized_keys
chmod 600 /root/.ssh/id_rsa*
yes | cp -f /vagrant/ci-cd/sshd_config /etc/ssh/
service sshd restart
swapoff -a

echo "Done!"
