
SysOps Assignment
========================
Technical Test
-------------------------

Please implement deployment of 3 tier application, which would run on Ubuntu server 18.04 LTS, would use Nginx, Postgres and Python code in consitent and repeatable way. You would need to automate deployment of:

1. Setup use of 10.0.0.2/18 static IP address, Netmask 255.255.0.0, gateway 10.0.0.1/18
2. Install Nginx, configure it to serve static pages and dynamic pages via FCGI (python application)
3. Install PostgreSQL DBMS and create DB, user for DB, set users password.
4. Install simple Python application wich would serve "Hello World!" via FCGI.
5. Make sure all your changes are persistent after reboot.

Bonus points:

- Use Vargant to spin up VM
- Use Ansible for server setup
___

Requirements
-------------------------

Vagrant, Virtualbox and Ansible are installed and configured.

Spin up VM
-------------------------

1. Copy ssh keys to ci-cd/ssh_keys folder.
2. Run `vagrunt up` command to spinn up VM. This will run `bento/ubuntu-18.04` and config passwordless authentication with your host.

```
$ vagrant up ubuntu-00
Bringing machine 'ubuntu-00' up with 'virtualbox' provider...
==> ubuntu-00: Importing base box 'bento/ubuntu-18.04'...
==> ubuntu-00: Matching MAC address for NAT networking...
==> ubuntu-00: Checking if box 'bento/ubuntu-18.04' is up to date...
==> ubuntu-00: Setting the name of the VM: vagrant_env_ubuntu-00_1574597317935_47809
==> ubuntu-00: Clearing any previously set network interfaces...
==> ubuntu-00: Preparing network interfaces based on configuration...
    ubuntu-00: Adapter 1: nat
    ubuntu-00: Adapter 2: hostonly
==> ubuntu-00: Forwarding ports...
    ubuntu-00: 22 (guest) => 10422 (host) (adapter 1)
==> ubuntu-00: Running 'pre-boot' VM customizations...
==> ubuntu-00: Booting VM...
==> ubuntu-00: Waiting for machine to boot. This may take a few minutes...
    ubuntu-00: SSH address: 127.0.0.1:10422
    ubuntu-00: SSH username: vagrant
    ubuntu-00: SSH auth method: private key
    ubuntu-00: Warning: Remote connection disconnect. Retrying...
    ubuntu-00: Warning: Remote connection disconnect. Retrying...
    ubuntu-00: 
    ubuntu-00: Vagrant insecure key detected. Vagrant will automatically replace
    ubuntu-00: this with a newly generated keypair for better security.
    ubuntu-00: 
    ubuntu-00: Inserting generated public key within guest...
    ubuntu-00: Removing insecure key from the guest if it's present...
    ubuntu-00: Key inserted! Disconnecting and reconnecting using new SSH key...
==> ubuntu-00: Machine booted and ready!
==> ubuntu-00: Checking for guest additions in VM...
    ubuntu-00: The guest additions on this VM do not match the installed version of
    ubuntu-00: VirtualBox! In most cases this is fine, but in rare cases it can
    ubuntu-00: prevent things such as shared folders from working properly. If you see
    ubuntu-00: shared folder errors, please make sure the guest additions within the
    ubuntu-00: virtual machine match the version of VirtualBox you have installed on
    ubuntu-00: your host and reload your VM.
    ubuntu-00: 
    ubuntu-00: Guest Additions Version: 6.0.14
    ubuntu-00: VirtualBox Version: 5.2
==> ubuntu-00: Setting hostname...
==> ubuntu-00: Configuring and enabling network interfaces...
==> ubuntu-00: Rsyncing folder: /home/alla/projects/SysOps/ => /vagrant
==> ubuntu-00:   - Exclude: [".vagrant/", ".git/"]
==> ubuntu-00: Running provisioner: shell...
    ubuntu-00: Running: /tmp/vagrant-shell20191124-1001-1qj9a5r.sh
    ubuntu-00: Done!
```

3. Rub Ansible playbook that will install docker, docker-compose and setup CI/CD that will trigger build and deploy on master branch update. 

```
$ sudo ansible-playbook ./ansible_hello.yaml

PLAY [all] ***************************************************************************************

TASK [Gathering Facts] ***************************************************************************************
ok: [ubuntu-00]

TASK [./ansible/roles/nickjj.docker : Disable pinned Docker version] ***************************************************************************************
ok: [ubuntu-00]

TASK [./ansible/roles/nickjj.docker : Enable pinned Docker version] ***************************************************************************************
skipping: [ubuntu-00]

TASK [./ansible/roles/nickjj.docker : Install Docker's dependencies] ***************************************************************************************
[WARNING]: Updating cache and auto-installing missing dependency: python-apt

changed: [ubuntu-00]

TASK [./ansible/roles/nickjj.docker : Add Docker's public GPG key to the APT keyring] ***************************************************************************************
changed: [ubuntu-00]

TASK [./ansible/roles/nickjj.docker : Configure Docker's upstream APT repository] ***************************************************************************************
changed: [ubuntu-00]

TASK [./ansible/roles/nickjj.docker : Install Docker] ***************************************************************************************
changed: [ubuntu-00]

TASK [./ansible/roles/nickjj.docker : Install Virtualenv] ***************************************************************************************
changed: [ubuntu-00]

TASK [./ansible/roles/nickjj.docker : Install Python packages] ***************************************************************************************
changed: [ubuntu-00] => (item={u'state': u'present', u'name': u'docker'})
changed: [ubuntu-00] => (item={u'path': u'/usr/local/bin/docker-compose', u'state': u'present', u'version': u'', u'name': u'docker-compose', u'src': u'/usr/local/lib/docker/virtualenv/bin/docker-compose'})

TASK [./ansible/roles/nickjj.docker : Symlink Python binary to /usr/local/bin/python-docker] ***************************************************************************************
changed: [ubuntu-00]

TASK [./ansible/roles/nickjj.docker : Symlink selected Python package binaries to /usr/local/bin] ***************************************************************************************
skipping: [ubuntu-00] => (item={u'state': u'present', u'name': u'docker'}) 
changed: [ubuntu-00] => (item={u'path': u'/usr/local/bin/docker-compose', u'state': u'present', u'version': u'', u'name': u'docker-compose', u'src': u'/usr/local/lib/docker/virtualenv/bin/docker-compose'})

TASK [./ansible/roles/nickjj.docker : Add user(s) to "docker" group] ***************************************************************************************
changed: [ubuntu-00] => (item=root)

TASK [./ansible/roles/nickjj.docker : Create Docker configuration directories] ***************************************************************************************
ok: [ubuntu-00] => (item=/etc/docker)
changed: [ubuntu-00] => (item=/etc/systemd/system/docker.service.d)

TASK [./ansible/roles/nickjj.docker : Configure Docker daemon options (json)] ***************************************************************************************
changed: [ubuntu-00]

TASK [./ansible/roles/nickjj.docker : Configure Docker daemon options (flags)] ***************************************************************************************
changed: [ubuntu-00]

TASK [./ansible/roles/nickjj.docker : Configure Docker daemon environment variables] ***************************************************************************************
skipping: [ubuntu-00]

TASK [./ansible/roles/nickjj.docker : Configure custom systemd unit file override] ***************************************************************************************
skipping: [ubuntu-00]

TASK [./ansible/roles/nickjj.docker : Reload systemd daemon] ***************************************************************************************
ok: [ubuntu-00]

RUNNING HANDLER [./ansible/roles/nickjj.docker : Restart Docker] ***************************************************************************************
changed: [ubuntu-00]

TASK [./ansible/roles/nickjj.docker : Manage Docker registry login credentials] ***************************************************************************************

TASK [./ansible/roles/nickjj.docker : Remove Docker related cron jobs] ***************************************************************************************
skipping: [ubuntu-00] => (item={u'job': u'docker system prune -af > /dev/null 2>&1', u'cron_file': u'docker-disk-clean-up', u'user': u'root', u'name': u'Docker disk clean up', u'schedule': [u'0', u'0', u'*', u'*', u'0']}) 

TASK [./ansible/roles/nickjj.docker : Create Docker related cron jobs] ***************************************************************************************
changed: [ubuntu-00] => (item={u'job': u'docker system prune -af > /dev/null 2>&1', u'cron_file': u'docker-disk-clean-up', u'user': u'root', u'name': u'Docker disk clean up', u'schedule': [u'0', u'0', u'*', u'*', u'0']})

TASK [./ansible/roles/setupCI : Copy init script] ***************************************************************************************
changed: [ubuntu-00]

TASK [./ansible/roles/setupCI : file] ***************************************************************************************
changed: [ubuntu-00]

TASK [./ansible/roles/setupCI : Execute the command in remote shell; stdout goes to the specified file on the remote.] ***************************************************************************************
changed: [ubuntu-00]

PLAY RECAP ***************************************************************************************
ubuntu-00                  : ok=20   changed=17   unreachable=0    failed=0    skipped=5    rescued=0    ignored=0 
```
___

Related articles
-------------------------
Python на nginx с использованием fcgiwrap

http://qaru.site/questions/1477937/python-on-nginx-using-fcgiwrap-upstream-closed-prematurely-fastcgi-stdout-while-reading-response-header-from-upstream

Sample app

https://github.com/tiangolo/uwsgi-nginx-docker/blob/master/python3.7-alpine3.9/Dockerfile

Setting up Push-to-Deploy with git

https://krisjordan.com/blog/2013/11/02/push-to-deploy-with-git

How to Install and Configure Ansible on Ubuntu 18.04

https://www.digitalocean.com/community/tutorials/how-to-install-and-configure-ansible-on-ubuntu-18-04

Ansible galaxy modules

https://galaxy.ansible.com/nickjj/docker

Markdown syntax

https://www.markdownguide.org/basic-syntax/